﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace webshop.common
{
	public static class StringExtentions
	{
		public static string UrlDecoded(this string str)
		{
			if (string.IsNullOrEmpty(str))
			{
				return str;
			}

			var bytes = HttpServerUtility.UrlTokenDecode(str);
			var chars = new char[bytes.Length / sizeof(char)];
			System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);

			return new string(chars);
		}

		public static string UrlEncoded(this string str)
		{
			if (string.IsNullOrEmpty(str))
			{
				return str;
			}

			var bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return HttpServerUtility.UrlTokenEncode(bytes);
		}
	}
}
