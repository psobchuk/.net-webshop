﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webshop.common
{
	public static class Constants
	{
		//COMMON CONSTANTS
		public const string CONNECTION_STRING_PROP = "DefaultConnection";
		public const string VAT_PROP = "VAT";
		public const string APPLICATION_ARTICLES = "articles_state";
		public const int DEFAULT_ITEMS_COUNT = 10;
		public const string DEFAULT_HANDLER_PATH = "/FileTransferHandler.ashx";
		public const string SESSION_CART_KEY = "sessioncartkey";

		//XML ARTICLES
		public const string ELEMENT_NAME = "article";
		public const string ELEMENT_TYPE_ATTR = "type";
		public const string NAME_NODE = "name";
		public const string COLOR_NODE = "color";
		public const string DISPLAY_NODE = "display";
		public const string CPU_NODE = "cpu";
		public const string HDD_NODE = "hdd";
		public const string PRICE_NODE = "price";
		public const string ELEMENT_IMG_ATTR = "img";
	}
}
