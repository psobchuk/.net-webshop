﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webshop.common
{
	public class ConfigManager
	{
		public static string ConnectionString
		{
			get
			{
				return ConfigurationManager.ConnectionStrings[Constants.CONNECTION_STRING_PROP].ConnectionString;
			}
		}

		public static double VAT
		{
			get
			{	
				double vatValue = 0.15;
				return  double.TryParse(ConfigurationManager.AppSettings[Constants.VAT_PROP], out vatValue) ? vatValue: 0.15;
			}
		}
	}
}
