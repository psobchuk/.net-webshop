﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace webshop.DataAccess
{
	public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class 
	{
		internal WebshopEntities context;
		internal DbSet<TEntity> dbSet;

		public GenericRepository(WebshopEntities context)
		{
			this.context = context;
			this.dbSet = context.Set<TEntity>();
		}

		public IQueryable<TEntity> All
		{
			get { return dbSet; }
		}

		public virtual IEnumerable<TEntity> Get(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			string includeProperties = "")
		{
			IQueryable<TEntity> query = dbSet;

			if (filter != null)
			{
				query = query.Where(filter);
			}

			foreach (var includeProperty in includeProperties.Split
				(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
			{
				query = query.Include(includeProperty);
			}

			if (orderBy != null)
			{
				return orderBy(query).ToList();
			}
			else
			{
				return query.ToList();
			}
		}

		/// <summary>
		/// Get Entity From Context
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		/// 
		public virtual TEntity FindByID(object id)
		{
			return dbSet.Find(id);
		}

		public virtual void Insert(TEntity entity)
		{
			dbSet.Add(entity);
		}

		/// <summary>
		/// Delete entity from context
		/// </summary>
		/// <param name="id"></param>
		public virtual void Delete(object id)
		{
			TEntity entityToDelete = dbSet.Find(id);
			Delete(entityToDelete);
		}

		/// <summary>
		/// Delete entity from context
		/// </summary>
		/// <param name="entityToDelete"></param>
		public virtual void Delete(TEntity entityToDelete)
		{
			if (context.Entry(entityToDelete).State == EntityState.Detached)
			{
				dbSet.Attach(entityToDelete);
			}
			dbSet.Remove(entityToDelete);
		}

		/// <summary>
		/// Update entity in context
		/// </summary>
		/// <param name="entityToUpdate"></param>
		public virtual void Update(TEntity entityToUpdate)
		{
			if (context.Entry(entityToUpdate).State != EntityState.Unchanged)
			{
				try
				{
					dbSet.Attach(entityToUpdate);
				}
				catch { }
				context.Entry(entityToUpdate).State = EntityState.Modified;

			}
		}

		public virtual void Update(TEntity sourceEntity, TEntity destEntity)
		{
			if (destEntity != null)
			{
				context.Entry(destEntity).CurrentValues.SetValues(sourceEntity);
				context.Entry(destEntity).State = EntityState.Modified;
			}
		}
	}
}
