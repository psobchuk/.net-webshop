namespace webshop.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address = c.String(),
                        HouseNumber = c.String(),
                        ZipCode = c.String(),
                        City = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        TotalPrice = c.Double(nullable: false),
                        Customer_CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.Customers", t => t.Customer_CustomerId, cascadeDelete: true)
                .Index(t => t.Customer_CustomerId);
            
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        OrderItemId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderItemId);
            
            CreateTable(
                "dbo.OrderOrderItems",
                c => new
                    {
                        Order_OrderId = c.Int(nullable: false),
                        OrderItem_OrderItemId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Order_OrderId, t.OrderItem_OrderItemId })
                .ForeignKey("dbo.Orders", t => t.Order_OrderId, cascadeDelete: true)
                .ForeignKey("dbo.OrderItems", t => t.OrderItem_OrderItemId, cascadeDelete: true)
                .Index(t => t.Order_OrderId)
                .Index(t => t.OrderItem_OrderItemId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "Customer_CustomerId", "dbo.Customers");
            DropForeignKey("dbo.OrderOrderItems", "OrderItem_OrderItemId", "dbo.OrderItems");
            DropForeignKey("dbo.OrderOrderItems", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.OrderOrderItems", new[] { "OrderItem_OrderItemId" });
            DropIndex("dbo.OrderOrderItems", new[] { "Order_OrderId" });
            DropIndex("dbo.Orders", new[] { "Customer_CustomerId" });
            DropTable("dbo.OrderOrderItems");
            DropTable("dbo.OrderItems");
            DropTable("dbo.Orders");
            DropTable("dbo.Customers");
        }
    }
}
