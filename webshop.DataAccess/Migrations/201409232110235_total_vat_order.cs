namespace webshop.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class total_vat_order : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "TotalVAT", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "TotalVAT");
        }
    }
}
