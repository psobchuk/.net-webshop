﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webshop.common;
using webshop.DataModel;

namespace webshop.DataAccess
{
	public class WebshopEntities: DbContext
	{
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<OrderItem> OrderItems { get; set; }

		public WebshopEntities(): base(ConfigManager.ConnectionString)
		{
			this.Configuration.LazyLoadingEnabled = true;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new CustomerTypeConfiguration());
			modelBuilder.Configurations.Add(new OrderTypeConfiguration());
			modelBuilder.Configurations.Add(new OrderItemTypeConfiguration());
			base.OnModelCreating(modelBuilder);
		}
	}
}
