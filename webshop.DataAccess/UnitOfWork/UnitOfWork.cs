﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webshop.DataModel;

namespace webshop.DataAccess
{
	public class UnitOfWork: IUnitOfWork
	{
		#region Private Members

		private WebshopEntities context = new WebshopEntities();		

		private GenericRepository<Customer> customerRepository;
		private GenericRepository<Order> orderRepository;
		private GenericRepository<OrderItem> orderItemRepository;

		#endregion

		public IRepository<Customer> CustomerRepository
		{
			get
			{
				return customerRepository ?? (customerRepository = new GenericRepository<Customer>(context));
			}
		}

		public IRepository<Order> OrderRepository
		{
			get
			{
				return orderRepository ?? (orderRepository = new GenericRepository<Order>(context));
			}
		}

		public IRepository<OrderItem> OrderItemRepository
		{
			get
			{
				return orderItemRepository ?? (orderItemRepository = new GenericRepository<OrderItem>(context));
			}
		}

		public void SaveChanges()
		{
			context.SaveChanges();
		}

		public void Dispose()
		{
			context.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}
