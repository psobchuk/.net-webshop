﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webshop.DataModel;

namespace webshop.DataAccess
{
	public interface IUnitOfWork: IDisposable
	{
		IRepository<Order> OrderRepository { get; }
		IRepository<Customer> CustomerRepository { get; }
		IRepository<OrderItem> OrderItemRepository { get; }
		void SaveChanges();
	}
}
