﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webshop.DataModel
{
	public class Order
	{
		public int OrderId { get; set; }
		public double TotalPrice { get; set; }
		public double TotalVAT { get; set; }
		public Customer Customer { get; set; }
		public List<OrderItem> OrderedItems { get; set; }
	}

	public class OrderTypeConfiguration : EntityTypeConfiguration<Order>
	{
		public OrderTypeConfiguration()
		{
			HasKey(k => k.OrderId);
			HasMany(p => p.OrderedItems).WithMany(p => p.Orders);
		}
	}
}
