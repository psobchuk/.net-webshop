﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webshop.DataModel
{
	public class Article
	{
		public int ArticleId { get; set; }
		public string Type { get; set; }
		public string Name { get; set; }
		public double Price { get; set; }
		public string HardDrive { get; set; }
		public string Display { get; set; }
		public string Color { get; set; }
		public string Cpu { get; set; }
		public string ImageUrl { get; set; }
	}
}
