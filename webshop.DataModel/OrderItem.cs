﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webshop.DataModel
{
	public class OrderItem
	{
		public int OrderItemId { get; set; }
		public string Name { get; set; }
		public int Price { get; set; }		
		public List<Order> Orders { get; set; }
	}

	public class OrderItemTypeConfiguration : EntityTypeConfiguration<OrderItem>
	{
		public OrderItemTypeConfiguration()
		{
			HasKey(k => k.OrderItemId);
		}
	}
}
