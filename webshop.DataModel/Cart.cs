﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webshop.DataModel
{
	public class Cart
	{
		public List<CartItem> CartItems { get; set; }
		public int TotalAmount { get; set; }
		public double TotalPrice { get; set; }
		public double TotalVAT { get; set; }

		public Cart()
		{
			CartItems = new List<CartItem>();
		}
	}
}
