﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webshop.DataModel
{
	public class CartItem
	{
		public int Id { get; set; }
		public double ItemPrice { get; set; }
		public string Name { get; set; }
		public int Amount { get; set; }
		public double TotalPrice { get; set; }
	}
}
