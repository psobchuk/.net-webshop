﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using webshop.common;
using webshop.DataModel;

namespace webshop.BusinessLogic {
	public class XmlArticlesManager: IArticlesManager
	{
		private const string DEFAULT_FILEPATH = "~/App_Data/articles.xml";
		
		private XmlDocument document = new XmlDocument();

		/// <summary>
		/// Stores articles data in application state
		/// </summary>
		private IEnumerable<Article> articles
		{
			get
			{
				HttpApplicationState appState = HttpContext.Current.Application;
				if (appState[Constants.APPLICATION_ARTICLES] == null)
				{
					appState[Constants.APPLICATION_ARTICLES] = LoadArticles();
				}

				return appState[Constants.APPLICATION_ARTICLES] as List<Article>;
			}
		}

		public XmlArticlesManager()
		{
			document.Load(HttpContext.Current.Server.MapPath(DEFAULT_FILEPATH));
		}

		public XmlArticlesManager(string filepath)
		{
			document.Load(filepath);
		}

		public List<Article> GetArticles()
		{
			return articles.ToList();
		}

		public List<Article> GetArticles(int pageIndex, int count)
		{
			return articles.Skip(pageIndex * count).Take(count).ToList();
		}

		/// <summary>
		/// Loads all articles from xml file into a list
		/// </summary>
		/// <returns>list of articles</returns>
		private List<Article> LoadArticles()
		{
			var articleList = new List<Article>();
			int idCounter = 0;
			foreach (XmlNode node in document.DocumentElement.ChildNodes)
			{
				var article = new Article();
				article.ArticleId = idCounter++;
				article.Type = node.Attributes[Constants.ELEMENT_TYPE_ATTR].Value;
				article.ImageUrl = string.Format("{0}?f={1}",Constants.DEFAULT_HANDLER_PATH, node.Attributes[Constants.ELEMENT_IMG_ATTR].Value.UrlEncoded());
				//parse each child node of article
				foreach (XmlNode childNode in node)
				{
					string innerText = childNode.InnerText;
					switch(childNode.Name.ToLower()) {
						case Constants.NAME_NODE:
							article.Name = innerText;
							break;
						case Constants.PRICE_NODE:
							article.Price = Convert.ToDouble(innerText);
							break;
						case Constants.HDD_NODE:
							article.HardDrive = innerText;
							break;
						case Constants.CPU_NODE:
							article.Cpu = innerText;
							break;
						case Constants.COLOR_NODE:
							article.Color = innerText;
							break;
					}
				}
				
				articleList.Add(article);
			}

			return articleList;
		}
	}
}
