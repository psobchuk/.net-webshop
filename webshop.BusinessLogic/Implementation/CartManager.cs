﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using webshop.common;
using webshop.DataModel;

namespace webshop.BusinessLogic
{
	public class CartManager: ICartManager
	{
		private Cart cart
		{
			get
			{
				HttpSessionState session = HttpContext.Current.Session;
				if (session[Constants.SESSION_CART_KEY] == null)
				{
					session[Constants.SESSION_CART_KEY] = new Cart();
				}

				return (Cart)session[Constants.SESSION_CART_KEY];
			}
		}

		public void AddItem(CartItem item)
		{
			CartItem cartItem = cart.CartItems.Where(p => p.Id == item.Id).SingleOrDefault();
			
			if (cartItem == null)
			{
				item.TotalPrice = item.ItemPrice;
				cart.CartItems.Add(item);
			}
			else
			{
				cartItem.Amount++;
				cartItem.TotalPrice += cartItem.ItemPrice;
			}
			
			cart.TotalAmount++;
			
		}

		public void RemoveItem(CartItem item)
		{
			CartItem cartItem = cart.CartItems.Where(p => p.Id == item.Id).SingleOrDefault();
			if (cartItem.Amount == 1)
			{
				cart.CartItems.Remove(cartItem);
			}
			else
			{
				cartItem.Amount--;
				cartItem.TotalPrice -= cartItem.ItemPrice; 
			}
			
			cart.TotalAmount--;
		}

		public void ClearCart()
		{
			HttpContext.Current.Session[Constants.SESSION_CART_KEY] = null;
		}

		public Cart GetCart()
		{
			cart.TotalPrice = GetTotalPrice();
			cart.TotalVAT = GetTotalVAT();
			return cart;
		}

		public double GetTotalPrice()
		{
			double totalPrice = 0;
			foreach (CartItem item in cart.CartItems)
			{
				totalPrice += item.TotalPrice;
			}

			return totalPrice;
		}

		public double GetTotalVAT()
		{
			return GetTotalPrice() * ConfigManager.VAT;
		}
	}
}
