﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webshop.DataAccess;
using webshop.DataModel;

namespace webshop.BusinessLogic
{
	public class OrderManager: IOrderManager
	{
		private IUnitOfWork unitOfWork;

		public OrderManager(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		public Order AddOrder(Order order)
		{
			unitOfWork.OrderRepository.Insert(order);
			unitOfWork.SaveChanges();

			return order;
		}
	}
}
