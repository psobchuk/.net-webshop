﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webshop.DataAccess;
using webshop.DataModel;

namespace webshop.BusinessLogic
{
	public class CustomerManager: ICustomerManager
	{
		private IUnitOfWork unitOfWork;

		public CustomerManager(IUnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		public Customer CheckoutCustomer(Cart cart, Customer customer)
		{
			var orderItems = AutoMapper.Mapper.Map<List<OrderItem>>(cart.CartItems);
			var order = new Order()
			{
				OrderedItems = orderItems,
				TotalPrice = cart.TotalPrice + cart.TotalVAT,
				TotalVAT = cart.TotalVAT
			};

			customer.Orders = new List<Order>();
			customer.Orders.Add(order);
			
			unitOfWork.CustomerRepository.Insert(customer);
			unitOfWork.SaveChanges();

			return customer;
		}
	}
}
