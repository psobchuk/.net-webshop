﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webshop.DataModel;

namespace webshop.BusinessLogic
{
	public interface ICustomerManager
	{
		Customer CheckoutCustomer(Cart cart, Customer customer);
	}
}
