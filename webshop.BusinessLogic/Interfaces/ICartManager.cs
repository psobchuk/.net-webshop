﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webshop.DataModel;

namespace webshop.BusinessLogic
{
	public interface ICartManager
	{
		void AddItem(CartItem item);
		void RemoveItem(CartItem item);
		void ClearCart();
		Cart GetCart();
		double GetTotalPrice();
		double GetTotalVAT();
	}
}
