﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using webshop.DIContainer;
using webshop.DataAccess;
using webshop.DataAccess.Migrations;

namespace webshop
{
	public class MvcApplication : System.Web.HttpApplication
	{
		private IWindsorContainer Container { get; set; }

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AutoMapperConfig.RegisterMappings();

			//initialize di container
			IocContainer.Setup();

			//initialize database migrations
			Database.SetInitializer( new MigrateDatabaseToLatestVersion<WebshopEntities, webshop.DataAccess.Migrations.Configuration>());
			new DbMigrator(new Configuration()).Update();
		}
	}
}