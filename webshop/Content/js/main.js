﻿var application = app = {
	appVM: null,
	vat: 0,

	init: function (model, cartModel) {
		this.appVM = new AppViewModel(model, cartModel);
		ko.applyBindings(this.appVM);
	}
}

var AppViewModel = function (model, cartModel) {
	var self = this;

	self.currPageIndex = 1;

	self.cart = new Cart(cartModel);
	self.articles = ko.observableArray(model);

	self.getMoreItems = function () {
		$.ajax({
			url: "/home/getarticles?pageIndex=" + self.currPageIndex,
			type: "GET",
			success: function (result) {
				for (var i = 0; i < result.length; i++) {
					self.articles.push(result[i]);
				}

				self.currPageIndex++;
			},
			error: function (err) {
				console.error("could not retrieve items");
			}
		});
	};

	self.showDetails = function (item) {
		$(".article[data-id=" + item.ArticleId + "] .article-details").slideToggle();
	};

	self.addToCart = function (item) {
		self.cart.addItem(item);
		$("#btnOpenCart").removeAttr("disabled");
	};

	self.openCart = function () {
		$("#cartModal").modal();
		if (self.cart.itemCount() > 0) {
			$("#btnCheckout").removeAttr("disabled");
		}
	}

	self.checkout = function () {
		$("#checkoutModal").modal();
	}
}

var Cart = function (model) {
	var self = this;

	self.itemCount = ko.observable(0);
	self.totalVat = ko.observable(0);
	self.cartItems = ko.observableArray();
	for (var i = 0; i < model.Items.length; i++) {
		self.cartItems.push(new CartItem(model.Items[i]));
		self.itemCount(self.itemCount() + model.Items[i].Amount);
	}
	
	self.addItem = function (item) {
		var cartItem = new CartItem(item);
		$.ajax({
			url: "/cart/addcartitem",
			type: "POST",
			data: cartItem,
			success: function (result) {
				if (result.Success) {
					self.onAddItem(item);
				} else {
					alert(result.ErrorMessage);
				}
			},
			error: function () {
				console.error("could not add item");
			}
		});
	}

	self.onAddItem = function (item) {
		var cartItem = $.grep(self.cartItems(), function (e) { return e.Id == item.ArticleId; });
		if (cartItem.length == 0) {
			self.cartItems.push(new CartItem(item));
		} else {
			var index = self.cartItems().indexOf(cartItem[0]);
			var amount = self.cartItems()[index].Amount();
			self.cartItems()[index].Amount(amount + 1);
		}

		self.itemCount(self.itemCount() + 1);
		$("#btnCheckout").removeAttr("disabled");
	};

	self.removeItem = function (item) {
		var cartItem = item;
		$.ajax({
			url: "/cart/removecartitem",
			type: "POST",
			data: cartItem,
			success: function (result) {
				if (result.Success) {
					self.onRemoveItem(item);
				} else {
					alert(result.ErrorMessage);
				}
			},
			error: function () {
				console.error("could not remove item");
			}
		});
	};

	self.onRemoveItem = function (item) {
		var cartItem = $.grep(self.cartItems(), function (e) { return e.Id == item.Id; });
		if (cartItem[0].Amount() > 1) {
			var index = self.cartItems().indexOf(cartItem[0]);
			var amount = self.cartItems()[index].Amount();
			self.cartItems()[index].Amount(amount - 1);
		} else {
			self.cartItems.remove(cartItem[0]);
		}

		self.itemCount(self.itemCount() - 1);
		if (self.itemCount() <= 0) {
			$("#btnCheckout, #btnOpenCart").attr("disabled", "disabled");
		}
	};

	self.totalPrice = ko.computed(function () {
		var price = 0;
		for (var i = 0; i < self.cartItems().length; i++) {
			price += self.cartItems()[i].TotalPrice();
		}
		
		self.totalVat(price * app.vat);
		return price + self.totalVat();
	});
}

var CartItem = function (item) {
	var self = this;

	self.ItemPrice = item.Price || item.ItemPrice;
	self.Id = item.ArticleId || item.Id;
	self.Name = item.Name;

	if (item.Amount) {
		self.Amount = ko.observable(item.Amount);
	} else {
		self.Amount = ko.observable(1);
	}

	self.TotalPrice = ko.computed(function () {
		return parseInt(self.ItemPrice) * self.Amount();
	});
}