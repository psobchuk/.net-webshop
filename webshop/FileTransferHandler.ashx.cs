﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using webshop.common;

namespace webshop
{
	/// <summary>
	/// Summary description for FileTransferHandler
	/// </summary>
	public class FileTransferHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.AddHeader("Pragma", "no-cache");
			context.Response.AddHeader("Cache-Control", "private, no-cache");
			if (context.Request.HttpMethod == "GET")
			{
				HandleGet(context);
			}
			else
			{
				context.Response.ClearHeaders();
				context.Response.StatusCode = 405;
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}

		private void HandleGet(HttpContext context)
		{
			if (context.Request["f"] != null)
			{
				string path = context.Request["f"].UrlDecoded();
				string filePath = HostingEnvironment.ApplicationPhysicalPath + path;
				if (File.Exists(filePath))
				{
					context.Response.ContentType = "image/jpeg";
					context.Response.WriteFile(filePath);
				}
				else
				{
					context.Response.StatusCode = (int)HttpStatusCode.NotFound;
				}
			}
			else
			{
				context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
			}
		}
	}
}