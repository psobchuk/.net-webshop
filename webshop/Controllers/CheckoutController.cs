﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webshop.BusinessLogic;
using webshop.DataModel;
using webshop.Models;

namespace webshop.Controllers
{
    public class CheckoutController : Controller
    {
		private IOrderManager orderManager;
		private ICustomerManager customerManager;
		private ICartManager cartManager;

		public CheckoutController(IOrderManager orderManager, ICustomerManager customerManager, ICartManager cartManager)
		{
			this.orderManager = orderManager;
			this.customerManager = customerManager;
			this.cartManager = cartManager;
		}

        public ActionResult Index()
        {
			var model = new CheckOutViewModel(cartManager.GetCart());
            return View(model);
        }

		[HttpPost]
		public ActionResult Index(CheckOutViewModel model)
		{
			if (ModelState.IsValid)
			{
				var customer = AutoMapper.Mapper.Map<Customer>(model.Customer);
				Cart shoppingCart = cartManager.GetCart();
				
				//checkout user and empty cart
				customerManager.CheckoutCustomer(shoppingCart, customer);
				cartManager.ClearCart();

				return RedirectToAction("ThankYou", "Home");
			}

			return View(model);
		}
    }
}
