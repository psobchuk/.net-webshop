﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webshop.BusinessLogic;
using webshop.DataModel;

namespace webshop.Controllers
{
    public class ArticlesController : ApiController
    {
		private IArticlesManager articlesManager;

		public ArticlesController(IArticlesManager articlesManager)
		{
			this.articlesManager = articlesManager;
		}

		/// <summary>
		/// Get all available articles
		/// </summary>
		/// <returns>List of articles</returns>
		public List<Article> Get()
		{
			return articlesManager.GetArticles();
		}
    }
}
