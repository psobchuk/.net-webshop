﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webshop.BusinessLogic;
using webshop.common;
using webshop.DataModel;
using webshop.Models;

namespace webshop.Controllers
{
    public class HomeController : Controller
    {
		private IArticlesManager articlesManager;
		private ICartManager cartManager;

		public HomeController(IArticlesManager articlesManager, ICartManager cartManager)
		{
			this.articlesManager = articlesManager;
			this.cartManager = cartManager;
		}

        public ActionResult Index()
        {
			var model = new ShopViewModel();
			model.Articles = articlesManager.GetArticles(0, Constants.DEFAULT_ITEMS_COUNT);
			model.VAT = ConfigManager.VAT;
			model.CartModel = new OrderViewModel(cartManager.GetCart());

			return View(model);
        }

		public ActionResult ThankYou()
		{
			return View();
		}

		public ActionResult ApiPage()
		{
			return View();
		}

		public JsonResult GetArticles(int pageIndex)
		{
			List<Article> articles = articlesManager.GetArticles(pageIndex, Constants.DEFAULT_ITEMS_COUNT);
			return Json(articles, JsonRequestBehavior.AllowGet);
		}
    }
}
