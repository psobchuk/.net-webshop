﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webshop.BusinessLogic;
using webshop.DataModel;
using webshop.Models;

namespace webshop.Controllers
{
    public class CartController : Controller
    {
		private ICartManager cartManager;
		public CartController(ICartManager cartManager)
		{
			this.cartManager = cartManager;
		}

		[HttpPost]
		public JsonResult AddCartItem(CartItem cartItem)
		{
			var response = new JsonResponse();
			try
			{
				cartManager.AddItem(cartItem);
				response.Success = true;
			}
			catch
			{
				response.Success = false;
				response.ErrorMessage = WebshopResources.Resources.AddCartItemError;
			}

			return Json(response);
		}

		[HttpPost]
		public JsonResult RemoveCartItem(CartItem cartItem)
		{
			var response = new JsonResponse();
			try
			{
				cartManager.RemoveItem(cartItem);
				response.Success = true;
			}
			catch
			{
				response.Success = false;
				response.ErrorMessage = WebshopResources.Resources.RemoveCartItemError;
			}

			return Json(response);
		}
    }
}
