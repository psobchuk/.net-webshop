﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webshop.DataModel;
using webshop.Models;

namespace webshop
{
	public class AutoMapperConfig
	{
		public static void RegisterMappings()
		{
			AutoMapper.Mapper.CreateMap<CustomerViewModel, Customer>();
			AutoMapper.Mapper.CreateMap<CartItem, OrderItem>()
				.ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.ItemPrice));
		}
	}
}