﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace webshop
{
	public class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/js/main").Include(
						"~/Content/ThirdParty/jquery/jquery-{version}.js",
						"~/Content/ThirdParty/bootstrap/js/bootstrap.js",
						"~/Content/ThirdParty/knockout/knockout.js",
						"~/Content/js/main.js"));

			bundles.Add(new StyleBundle("~/Content/css/main").Include(
					  "~/Content/ThirdParty/bootstrap/css/bootstrap.css",
					  "~/Content/css/main.css"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Content/ThirdParty/jquery/jquery.unobtrusive*",
						"~/Content/ThirdParty/jquery/jquery.validate*"));
		}
	}
}