﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webshop.Models
{
	public class CustomerViewModel
	{
		[Required]
		public string Title { get; set; }
		[Required]
		public string FirstName { get; set; }
		[Required]
		public string LastName { get; set; }
		[Required]
		public string Address { get; set; }
		[Required]
		public string HouseNumber { get; set; }
		[Required]
		public string ZipCode { get; set; }
		[Required]
		public string City { get; set; }
		[Required]
		[RegularExpression(@"[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}")]
		public string Email { get; set; }
	}
}