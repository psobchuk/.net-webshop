﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webshop.Models
{
	public class JsonResponse
	{
		public bool Success { get; set; }
		public object Data { get; set; }
		public string ErrorMessage { get; set; }
	}
}