﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webshop.BusinessLogic;
using webshop.common;
using webshop.DataModel;

namespace webshop.Models
{
	public class OrderViewModel
	{
		public List<CartItem> Items { get; set; }
		public double TotalPrice { get; set; }
		public double TotalPriceVAT { get; set; }
		public double TotalVAT { get; set; }

		public OrderViewModel(Cart cart)
		{
			Items = cart.CartItems;
			TotalPrice = cart.TotalPrice;
			TotalVAT = cart.TotalVAT;
			TotalPriceVAT = TotalPrice + TotalVAT;
		}
	}
}