﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webshop.BusinessLogic;
using webshop.DataModel;

namespace webshop.Models
{
	public class CheckOutViewModel
	{
		public CustomerViewModel Customer { get; set; }
		public OrderViewModel Order { get; set; }

		public CheckOutViewModel() { }

		public CheckOutViewModel(Cart cart)
		{
			Customer = new CustomerViewModel();
			Order = new OrderViewModel(cart);
		}
	}
}