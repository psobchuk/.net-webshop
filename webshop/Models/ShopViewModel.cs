﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using webshop.DataModel;

namespace webshop.Models
{
	public class ShopViewModel
	{
		public List<Article> Articles { get; set; }
		public double VAT { get; set; }
		public OrderViewModel CartModel { get; set; }
	}
}