﻿using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;

namespace webshop.DIContainer
{
	public static class IocContainer
	{
		private static IWindsorContainer container;

		public static void Setup()
		{
			container = new WindsorContainer().Install(FromAssembly.This());

			//Custom factory for web controllers
			WindsorControllerFactory controllerFactory = new WindsorControllerFactory(container.Kernel);
			ControllerBuilder.Current.SetControllerFactory(controllerFactory);

			//Custom controller activator for api controllers
			GlobalConfiguration.Configuration.Services.Replace(
			typeof(IHttpControllerActivator), new WindsorControllerActivator(container));
		}
	}
}