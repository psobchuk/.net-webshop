﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using webshop.BusinessLogic;
using webshop.Controllers;
using webshop.DataAccess;

namespace webshop.DIContainer
{
	public class DependencyConventions: IWindsorInstaller
	{
		public DependencyConventions() { }

		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(Classes.FromThisAssembly()
				.Pick().If(t => t.Name.EndsWith("Controller"))
				.Configure(configurer => configurer.Named(configurer.Implementation.Name))
				.LifestylePerWebRequest());

			container.Register(Component.For<IArticlesManager>().ImplementedBy<XmlArticlesManager>().LifestyleTransient());
			container.Register(Component.For<ICartManager>().ImplementedBy<CartManager>().LifestyleTransient());
			container.Register(Component.For<ICustomerManager>().ImplementedBy<CustomerManager>().LifestyleTransient());
			container.Register(Component.For<IOrderManager>().ImplementedBy<OrderManager>().LifestyleTransient());
			container.Register(Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient());
		}
	}
}
